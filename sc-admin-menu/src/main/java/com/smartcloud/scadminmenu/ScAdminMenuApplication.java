package com.smartcloud.scadminmenu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
@MapperScan("com.smartcloud.scadminmenu.dao")
public class ScAdminMenuApplication {
	@RequestMapping("/hi")
	public String hi() {
		return "hi, I'm sc-admin-menu2";
	}

	public static void main(String[] args) {
		SpringApplication.run(ScAdminMenuApplication.class, args);
	}
}
