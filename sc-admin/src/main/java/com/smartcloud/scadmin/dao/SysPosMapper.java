package com.smartcloud.scadmin.dao;

import com.smartcloud.scadmin.model.SysPos;
import com.smartcloud.scadmin.model.SysPosExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysPosMapper {
    long countByExample(SysPosExample example);

    int deleteByExample(SysPosExample example);

    int deleteByPrimaryKey(String posId);

    int insert(SysPos record);

    int insertSelective(SysPos record);

    List<SysPos> selectByExample(SysPosExample example);

    SysPos selectByPrimaryKey(String posId);

    int updateByExampleSelective(@Param("record") SysPos record, @Param("example") SysPosExample example);

    int updateByExample(@Param("record") SysPos record, @Param("example") SysPosExample example);

    int updateByPrimaryKeySelective(SysPos record);

    int updateByPrimaryKey(SysPos record);
}