package com.smartcloud.scadmin.dao;

import com.smartcloud.scadmin.model.SysUserPos;
import com.smartcloud.scadmin.model.SysUserPosExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysUserPosMapper {
    long countByExample(SysUserPosExample example);

    int deleteByExample(SysUserPosExample example);

    int deleteByPrimaryKey(String id);

    int insert(SysUserPos record);

    int insertSelective(SysUserPos record);

    List<SysUserPos> selectByExample(SysUserPosExample example);

    SysUserPos selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") SysUserPos record, @Param("example") SysUserPosExample example);

    int updateByExample(@Param("record") SysUserPos record, @Param("example") SysUserPosExample example);

    int updateByPrimaryKeySelective(SysUserPos record);

    int updateByPrimaryKey(SysUserPos record);
}