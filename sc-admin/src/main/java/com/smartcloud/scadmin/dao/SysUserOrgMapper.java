package com.smartcloud.scadmin.dao;

import com.smartcloud.scadmin.model.SysUserOrg;
import com.smartcloud.scadmin.model.SysUserOrgExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SysUserOrgMapper {
    long countByExample(SysUserOrgExample example);

    int deleteByExample(SysUserOrgExample example);

    int deleteByPrimaryKey(String id);

    int insert(SysUserOrg record);

    int insertSelective(SysUserOrg record);

    List<SysUserOrg> selectByExample(SysUserOrgExample example);

    SysUserOrg selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") SysUserOrg record, @Param("example") SysUserOrgExample example);

    int updateByExample(@Param("record") SysUserOrg record, @Param("example") SysUserOrgExample example);

    int updateByPrimaryKeySelective(SysUserOrg record);

    int updateByPrimaryKey(SysUserOrg record);
}