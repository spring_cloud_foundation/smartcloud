package com.smartcloud.scadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@RestController
@EnableTransactionManagement
@EnableSwagger2 //api地址：http://localhost:8800/swagger-ui.html
@MapperScan("com.smartcloud.scadmin.dao")
public class ScAdminApplication {
	@RequestMapping("/hi")
	public String home() {
		return "hi, I'm admin 1";
	}
	public static void main(String[] args) {
		SpringApplication.run(ScAdminApplication.class, args);
	}
}
