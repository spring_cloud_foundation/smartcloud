package com.smartcloud.scadmin.model;

public class SysUserOrg {
    private String id;

    private String orgId;

    private String userId;

    private Integer isPrimaryOrg;

    private Integer isOrgLeader;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getIsPrimaryOrg() {
        return isPrimaryOrg;
    }

    public void setIsPrimaryOrg(Integer isPrimaryOrg) {
        this.isPrimaryOrg = isPrimaryOrg;
    }

    public Integer getIsOrgLeader() {
        return isOrgLeader;
    }

    public void setIsOrgLeader(Integer isOrgLeader) {
        this.isOrgLeader = isOrgLeader;
    }
}