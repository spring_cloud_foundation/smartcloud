package com.smartcloud.scadmin.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * @author zhangda
 * swagger2配置
 *
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        System.out.println("-------------------------------------------------------");
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("排班服务api文档--张达")
                        .description("排班服务api文档——简单优雅的restful风格")
                        .termsOfServiceUrl("http://localhost:8099/")
                        .version("1.1")
                        .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.zhangda.myshift"))
                .paths(PathSelectors.any())
                .build();
    }

}
