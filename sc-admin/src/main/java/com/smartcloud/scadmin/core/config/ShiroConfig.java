package com.smartcloud.scadmin.core.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.Filter;

import com.smartcloud.scadmin.core.shiro.MyFormAuthenticationFilter;
import com.smartcloud.scadmin.core.shiro.MyRealm;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.session.mgt.ExecutorServiceSessionValidationScheduler;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.filter.DelegatingFilterProxy;


@Configuration
public class ShiroConfig {

//    @Bean(name = "shiroEhcacheManager")
//    public EhCacheManager getEhCacheManager() {
//        EhCacheManager em = new EhCacheManager();
//        em.setCacheManagerConfigFile("classpath:ehcache-shiro.xml");
//        return em;
//    }

//    @Bean(name = "lifecycleBeanPostProcessor")
//    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
//        LifecycleBeanPostProcessor lifecycleBeanPostProcessor = new LifecycleBeanPostProcessor();
//        return lifecycleBeanPostProcessor;
//    }
//
//    @Bean(name = "sessionValidationScheduler")
//    public ExecutorServiceSessionValidationScheduler getExecutorServiceSessionValidationScheduler() {
//        ExecutorServiceSessionValidationScheduler scheduler = new ExecutorServiceSessionValidationScheduler();
//        scheduler.setInterval(900000);
//        //scheduler.setSessionManager(defaultWebSessionManager());
//        return scheduler;
//    }
//
//    @Bean
//    public HashedCredentialsMatcher hashedCredentialsMatcher() {
//        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
//        hashedCredentialsMatcher.setHashAlgorithmName("md5");// 散列算法:这里使用MD5算法;
//        hashedCredentialsMatcher.setHashIterations(1);// 散列的次数，比如散列两次，相当于md5(md5(""));
//        return hashedCredentialsMatcher;
//    }
//
//    @Bean(name = "defaultWebSecurityManager")
//    public DefaultWebSecurityManager defaultWebSecurityManager(MyRealm myRealm, DefaultWebSessionManager defaultWebSessionManager) {
//        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
//        defaultWebSecurityManager.setRealm(myRealm);
//        defaultWebSecurityManager.setCacheManager(getEhCacheManager());
//        defaultWebSecurityManager.setSessionManager(defaultWebSessionManager);
//        defaultWebSecurityManager.setRememberMeManager(rememberMeManager());
//        return defaultWebSecurityManager;
//    }
//
//    @Bean(name = "rememberMeCookie")
//    public SimpleCookie rememberMeCookie() {
//        // 这个参数是cookie的名称，对应前端的checkbox的name = rememberMe
//        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
//        // <!-- 记住我cookie生效时间30天 ,单位秒;-->
//        simpleCookie.setMaxAge(259200);
//        return simpleCookie;
//    }
//
//    /**
//     * cookie管理对象;
//     *
//     * @return
//     */
//    @Bean(name = "rememberMeManager")
//    public CookieRememberMeManager rememberMeManager() {
//        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
//        cookieRememberMeManager.setCookie(rememberMeCookie());
//        return cookieRememberMeManager;
//    }
//
//    @Bean
//    @DependsOn(value = "lifecycleBeanPostProcessor")
//    public MyRealm myRealm() {
//        MyRealm myRealm = new MyRealm();
//        myRealm.setCacheManager(getEhCacheManager());
//        myRealm.setCredentialsMatcher(hashedCredentialsMatcher());
//        return myRealm;
//    }
//
//    @Bean
//    @DependsOn("lifecycleBeanPostProcessor")
//    public DefaultAdvisorAutoProxyCreator getAutoProxyCreator(){
//        DefaultAdvisorAutoProxyCreator creator = new DefaultAdvisorAutoProxyCreator();
//        creator.setProxyTargetClass(true);
//        return creator;
//    }
//
//    @Bean
//    public AuthorizationAttributeSourceAdvisor getAuthorizationAttributeSourceAdvisor(DefaultWebSecurityManager defaultWebSecurityManager) {
//        AuthorizationAttributeSourceAdvisor aasa = new AuthorizationAttributeSourceAdvisor();
//        aasa.setSecurityManager(defaultWebSecurityManager);
//        return aasa;
//    }
//
//    @Bean(name = "sessionManager")
//    public DefaultWebSessionManager defaultWebSessionManager() {
//        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
//        sessionManager.setGlobalSessionTimeout(18000000);
////		//url中是否显示session Id
//        sessionManager.setSessionIdUrlRewritingEnabled(false);
////		// 删除失效的session
//        sessionManager.setDeleteInvalidSessions(true);
//        sessionManager.setSessionValidationSchedulerEnabled(true);
//        sessionManager.setSessionValidationInterval(18000000);
//        sessionManager.setSessionValidationScheduler(getExecutorServiceSessionValidationScheduler());
//        //设置SessionIdCookie 导致认证不成功，不从新设置新的cookie,从sessionManager获取sessionIdCookie
//        //sessionManager.setSessionIdCookie(simpleIdCookie());
//        sessionManager.getSessionIdCookie().setName("session-z-id");
//        sessionManager.getSessionIdCookie().setPath("/");
//        sessionManager.getSessionIdCookie().setMaxAge(60*60*24*7);
//
//
//
//        return sessionManager;
//    }
//
//    @Bean(name = "filterRegistrationBean1")
//    public FilterRegistrationBean filterRegistrationBean() {
//        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
//        filterRegistrationBean.setFilter(new DelegatingFilterProxy(
//                "shiroFilter"));
//        filterRegistrationBean
//                .addInitParameter("targetFilterLifecycle", "true");
//        filterRegistrationBean.setEnabled(true);
//        filterRegistrationBean.addUrlPatterns("/");
//        return filterRegistrationBean;
//    }
//
//    @Bean(name = "shiroFilter")
//    public ShiroFilterFactoryBean shiroFilterFactoryBean(
//            @Qualifier("defaultWebSecurityManager") DefaultWebSecurityManager defaultWebSecurityManager) {
//        // SecurityUtils.setSecurityManager(defaultWebSecurityManager);
//        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
//        shiroFilterFactoryBean.setLoginUrl("/login");
//        shiroFilterFactoryBean.setSuccessUrl("/getMyJsp");
//        shiroFilterFactoryBean.setUnauthorizedUrl("/login");
//        shiroFilterFactoryBean.setSecurityManager(defaultWebSecurityManager);
//        Map<String, Filter> filterMap1 = shiroFilterFactoryBean.getFilters();
//        //自定义的filter 不能交给spring 容器管理，只能使用new 实例化filter
//        filterMap1.put("authc", new MyFormAuthenticationFilter());
//        shiroFilterFactoryBean.setFilters(filterMap1);
//        Map<String, String> filterMap = new LinkedHashMap<String, String>();
//        filterMap.put("/static/**", "anon");
//        filterMap.put("/logout", "logout");
//        filterMap.put("/login", "authc");
//        filterMap.put("/**", "authc");
//        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);
//        return shiroFilterFactoryBean;
//
//    }

}
