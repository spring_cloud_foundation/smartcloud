package com.smartcloud.scadmin.service;

import com.smartcloud.scadmin.dao.SysUserMapper;
import com.smartcloud.scadmin.model.SysUser;
import com.smartcloud.scadmin.model.SysUserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysUserService {
	
	@Autowired
	private SysUserMapper sysUserMapper;

	public int insert(SysUser sysUser){
		return sysUserMapper.insert(sysUser);
	}
	
//	public List<SysTable> selectAll(SysUser sysUser, int pageNo, int pageSize){
//		PageHelper.startPage(pageNo, pageSize);
//		return sysUserMapper.selectAll();
//	}
	
	public int updateByPrimaryKeySelective(SysUser sysUser){
		return sysUserMapper.updateByPrimaryKeySelective(sysUser);
	}
	
	public int deleteByPrimaryKey(String id){
		return sysUserMapper.deleteByPrimaryKey(id);
	}
	
	public List<SysUser> selectByExample(SysUserExample example){
		return sysUserMapper.selectByExample(example);
	}
	
}
