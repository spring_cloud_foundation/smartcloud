package com.smartcloud.scadmintable.model;

import javax.persistence.Id;
import javax.persistence.Table;

@Table(name="sys_table_column")
public class SysTableColumn {
	
	@Id
    private String id;

    private String tableId;

    private String name;

    private String description;

    private Integer columnIndex;

    private String columnType;

    private Integer columnLength;

    private Integer columnDecimal;

    private String defaultValue;

    private String isPrimeryKey;

    private String isAllowNull;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(Integer columnIndex) {
        this.columnIndex = columnIndex;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public Integer getColumnLength() {
        return columnLength;
    }

    public void setColumnLength(Integer columnLength) {
        this.columnLength = columnLength;
    }

    public Integer getColumnDecimal() {
        return columnDecimal;
    }

    public void setColumnDecimal(Integer columnDecimal) {
        this.columnDecimal = columnDecimal;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getIsPrimeryKey() {
        return isPrimeryKey;
    }

    public void setIsPrimeryKey(String isPrimeryKey) {
        this.isPrimeryKey = isPrimeryKey;
    }

    public String getIsAllowNull() {
        return isAllowNull;
    }

    public void setIsAllowNull(String isAllowNull) {
        this.isAllowNull = isAllowNull;
    }
}