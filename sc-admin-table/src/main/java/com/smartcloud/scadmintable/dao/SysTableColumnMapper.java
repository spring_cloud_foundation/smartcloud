package com.smartcloud.scadmintable.dao;

import com.smartcloud.scadmintable.model.SysTableColumn;
import tk.mybatis.mapper.common.Mapper;

public interface SysTableColumnMapper extends Mapper<SysTableColumn>{
    
}