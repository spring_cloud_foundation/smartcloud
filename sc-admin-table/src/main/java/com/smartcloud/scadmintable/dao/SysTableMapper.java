package com.smartcloud.scadmintable.dao;

import com.github.pagehelper.Page;
import com.smartcloud.scadmintable.model.SysTable;
import tk.mybatis.mapper.common.Mapper;

public interface SysTableMapper extends Mapper<SysTable>{
    int deleteByPrimaryKeys(String[] ids);
    
    Page<SysTable> selectAllWithList(SysTable record);
}