package com.smartcloud.common.util;

import java.util.UUID;

public class Guid {
	
	/**
	 * 生成GUID
	 * @return
	 */
	public static String newGuid() {
		
		return UUID.randomUUID().toString().toUpperCase();
	}
	
	public static void main(String[] args) {
		System.out.println(newGuid().length());
	}
}
