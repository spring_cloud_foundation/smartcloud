package com.smartcloud.common.util;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

/***
 * 返回json对象封装类（属性为null，不返回）
 * @author zhangda
 *
 */

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class ResultMessage {
	public final static boolean SUCCESS = true;
	public final static boolean ERROR = false;

	public ResultMessage(boolean isSuccess){
		this.success = isSuccess;
	}
	
	public ResultMessage(boolean isSuccess, String msg){
		this.success = isSuccess;
		this.msg = msg;
	}
	
	public ResultMessage(boolean isSuccess, String msg, String errorCode){
		this.success = isSuccess;
		this.msg = msg;
		this.errorCode = errorCode;
	}
	
	public ResultMessage(boolean isSuccess, List list){
		this.success = isSuccess;
		this.list = list;
	}
	
	public ResultMessage(boolean isSuccess, Integer totalCount, List list){
		this.success = isSuccess;
		this.totalCount = totalCount;
		this.list = list;
	}
	
	public ResultMessage(boolean isSuccess, Object object){
		this.success = isSuccess;
		this.object = object;
	}
	
	private boolean success;
	private String msg;
	private String errorCode;
	private List list;
	private Object object;
	private Integer totalCount;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	
}
