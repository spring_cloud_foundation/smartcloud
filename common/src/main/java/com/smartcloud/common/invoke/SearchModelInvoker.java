package com.smartcloud.common.invoke;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;


public class SearchModelInvoker {

	public static Object invoke(Object model, Object example) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Field[] field = model.getClass().getDeclaredFields();//获取实体类的所有属性，返回Field数组  
		Object criteria = example.getClass().getMethod("createCriteria").invoke(example);
		for(Field fi : field){
			String fieldName = fi.getName();    //获取属性的名字
			fieldName = fieldName.substring(0,1).toUpperCase() + fieldName.substring(1);  //将属性的首字符大写，方便构造get，set方法
			Object value = model.getClass().getMethod("get"+fieldName).invoke(model);
			if(value != null){
				String fieldType = fi.getGenericType().toString(); 
				if(fieldType.equals("class java.lang.String")){
					criteria.getClass().getMethod("and" + fieldName +"Like", fi.getType()).invoke(criteria, "%" + value + "%");
				}else {
					criteria.getClass().getMethod("and" + fieldName +"EqualTo", fi.getType()).invoke(criteria, value);
				}
				
			}
		}
		
		
		return example;
	}
}
