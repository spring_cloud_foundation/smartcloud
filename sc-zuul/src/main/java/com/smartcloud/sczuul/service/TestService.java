package com.smartcloud.sczuul.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Description:    feign测试调用service
 * @Author:         zhangda
 * @CreateDate:     2018/8/5 22:17
 * @Version:        1.0
 */

@FeignClient("admin")
public interface TestService {
    @GetMapping("/hi")
    String getHi();
}
