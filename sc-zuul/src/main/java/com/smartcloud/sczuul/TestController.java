package com.smartcloud.sczuul;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TestController {
    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/test")
    public String getHi(){
        return restTemplate.getForObject("http://admin/hi", String.class);
    }
}
