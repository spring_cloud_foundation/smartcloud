package com.smartcloud.sczuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.smartcloud.sczuul.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;

/**
* @Description:
* @Author:         zhangda
* @CreateDate:     2018/8/5 21:49
* @Version:        1.0
*/
public class AccessFilter extends ZuulFilter {
    @Autowired
    TestService testService;


    private String ignorePath = "/api-admin/login";

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 10000;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }


    @Override
    public Object run() {
//        String hi = testService.getHi();
        return null;
//        RequestContext ctx = RequestContext.getCurrentContext();
//        HttpServletRequest request = ctx.getRequest();
//        final String requestUri = request.getRequestURI();
//        if (isStartWith(requestUri)) {
//            return null;
//        }
//        String accessToken = request.getHeader(CommonConstants.CONTEXT_TOKEN);
//        if(null == accessToken || accessToken == ""){
//            accessToken = request.getParameter(CommonConstants.TOKEN);
//        }
//        if (null == accessToken) {
//            setFailedRequest(R.error401(), 200);
//            return null;
//        }
//        try {
//            UserToken userToken = JwtUtils.getInfoFromToken(accessToken);
//        } catch (Exception e) {
//            setFailedRequest(R.error401(), 200);
//            return null;
//        }
//        FilterContextHandler.setToken(accessToken);
//        if(!havePermission(request)){
//            setFailedRequest(R.error403(), 200);
//            return null;
//        }
//        Set<String> headers = (Set<String>) ctx.get("ignoredHeaders");
//        //We need our JWT tokens relayed to resource servers
//        //添加自己header
////        ctx.addZuulRequestHeader(CommonConstants.CONTEXT_TOKEN, accessToken);
//        //移除忽略token
//        headers.remove("authorization");
//        return null;
////        RequestContext ctx = RequestContext.getCurrentContext();
////        Set<String> headers = (Set<String>) ctx.get("ignoredHeaders");
////        // We need our JWT tokens relayed to resource servers
////        headers.remove("authorization");
////        return null;
    }

}
